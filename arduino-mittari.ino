/*

 Arduino TFT Bitmap Logo example

 This example reads an image file from a micro-SD card
 and draws it on the screen, at random locations.

 In this sketch, the Arduino logo is read from a micro-SD card.
 There is a .bmp file included with this sketch.
 - open the sketch folder (Ctrl-K or Cmd-K)
 - copy the "arduino.bmp" file to a micro-SD
 - put the SD into the SD slot of the Arduino TFT module.

 This example code is in the public domain.

 Created 19 April 2013 by Enrico Gueli

 http://www.arduino.cc/en/Tutorial/TFTBitmapLogo

 */

// include the necessary libraries
#include <SPI.h>
#include <SD.h>
#include <TFT.h>  // Arduino LCD library

// pin definition for the Uno
#define lcd_cs 10
#define dc     9
#define rst    8
#define sd_cs  4

TFT TFTscreen = TFT(lcd_cs, dc, rst);

// char array to print to the screen
char tempSensorPrintout[4];
char luxSensorPrintout[4];

// this variable represents the image to be drawn on screen
//PImage logo;

void setup() {
  analogWrite(5, 100);
  // initialize the serial port: it will be used to
  // print some diagnostic info
  Serial.begin(9600);
  while (!Serial) {
    // wait for serial port to connect. Needed for native USB port only
  }
  
  // initialize the GLCD and show a message
  // asking the user to open the serial line
  TFTscreen.begin();
  // clear the screen with balck background
  TFTscreen.background(0, 0, 0);

  // write the static text to the screen
  // set the font color to white
  TFTscreen.stroke(255, 255, 255);
  TFTscreen.fill(0, 0, 0);
  // set the font size
  TFTscreen.setTextSize(2);
  // write the text to the top left corner of the screen
  TFTscreen.text("Sensor values :\n ", 0, 0);
  // set the font size very large for the loop
  TFTscreen.setTextSize(4);

  Serial.println("LCD screen dimensions: \nheight:" + String(TFTscreen.height()) + "\nwidth:" + String(TFTscreen.width()));
/* 
  // try to access the SD card. If that fails (e.g.
  // no card present), the setup process will stop.
  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(sd_cs)) {
    Serial.println(F("failed!"));
    return;
  } */
  Serial.println(F("OK!"));
}

void loop() {
  // Read the value of the sensor A0
  String tempSensorVal = String(analogRead(A0));
  String luxSensorVal = String(analogRead(A1));

  // convert the reading to a char array
  tempSensorVal.toCharArray(tempSensorPrintout, 4);
  luxSensorVal.toCharArray(luxSensorPrintout, 4);

  // set the font color 
  TFTscreen.stroke(255, 255, 255);
  // print the sensor value
  TFTscreen.text(tempSensorPrintout, 0, 20);
  TFTscreen.text(luxSensorPrintout, 0, 40);
  // wait for a moment
  delay(500);
  // erase the text you just wrote
  TFTscreen.stroke(0, 0, 0);
  TFTscreen.rect(0 ,20 , TFTscreen.width()-1, TFTscreen.height()-1);
  //TFTscreen.text(tempSensorPrintout, 0, 20);
  //TFTscreen.text(luxSensorPrintout, 0, 60);
}
